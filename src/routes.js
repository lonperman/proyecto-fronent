import React from 'react';

const Compra = React.lazy(() => import('./views/Carros/Compra_Carro'));
const Venta = React.lazy(() => import('./views/Carros/Venta_Carro'));
const Registro_Carro = React.lazy(() => import('./views/Carros/Ingresar_Carro'));
const Publicaciones = React.lazy(() => import('./views/Carros/Publicaciones'));
const Charts = React.lazy(() => import('./views/Charts'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Alerts = React.lazy(() => import('./views/Notifications/Alerts'));
const Badges = React.lazy(() => import('./views/Notifications/Badges'));
const Modals = React.lazy(() => import('./views/Notifications/Modals'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/Carros/Compra_Carro', exact: true, name: 'Compra', component: Compra },
  /*{ path: '/Carros', exact: true, name: 'Carros', component: Venta },*/
  { path: '/Carros/Ingresar_Carro', exact: true, name: 'Registro Carro', component: Registro_Carro },
  { path: '/Carros/Venta_Carro', name: 'Venta', component: Venta },
  { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
];

export default routes;
