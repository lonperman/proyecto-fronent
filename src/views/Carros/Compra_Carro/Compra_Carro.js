import React, { Component } from 'react';
import axios from 'axios';
import {Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
 } from 'reactstrap';

class Carro_Compra extends Component {

  placa = React.createRef();
  dueño = React.createRef();
  precio = React.createRef();

  constructor(props){
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      Collapse: true,
      fadeIn: true,
      timeout: 300
    };
  }
    toggle() {
      this.setState({ collapse: !this.state.collapse});
    }

    toggleFade(){
      this.setState((prevState) => { return  { fadeIn: !prevState}});
    }

   

    Crear_Compra = (e) => {
      e.preventDefault();

      const compra_c = {
        placa : this.placa.current.value,
        dueño : this.dueño_compra.current.value,
        precio : this.precio.current.value

    }
     
    axios.post(`http://localhost:4552/api/conferencistas/crear`,compra_c)
    .then(res =>{

    })

    console.log(compra_c);


    }
  
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6">
            <CardHeader>
              <strong>Formulario De Compra</strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col xs="12">
                  <FormGroup onSubmit={this.Crear_Compra}>
                    <Label htmlFor="placa">Placa</Label>
                    <Input type="select" id="placa" ref={this.placa}>
                    <option value="1" >1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="dueño_compra">Dueño Carro</Label>
                    <Input type="select" id="dueño_compra" ref={this.dueño_compra}>
                    <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="precio_compra">Precio Compra</Label>
                    <Input type="text" id="precio_compra" ref={this.precio_compra} >
                    
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i>Submit</Button>
            </CardFooter>
          </Col>
        </Row>
     
      </div>

    );
  }
}

export default Carro_Compra;
