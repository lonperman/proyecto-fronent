import React from 'react';
import ReactDOM from 'react-dom';
import Buttons from './Buttons';
import Venta_Carro from './Venta_Carro';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Venta_Carro />, div);
  ReactDOM.unmountComponentAtNode(div);
});
